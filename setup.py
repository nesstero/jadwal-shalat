#!/usr/bin/env python
from setuptools import find_packages, setup

with open("README.md", "r") as des:
    l_desc = des.read()

setup(
    name='jadwal_shalat',
    packages=find_packages(),
    version='0.0.2',
    entry_points={'console_scripts': ['jadwal-shalat = jadwal_shalat:display']},
    description='Simple Jadwal Shalat',
    url='https://gitlab.com/nesstero/jadwal-shalat',
    license='MIT',
    author='nestero',
    author_email='nestero@mail.com',
    long_description=l_desc,
    long_description_content_type='text/markdown',
)
